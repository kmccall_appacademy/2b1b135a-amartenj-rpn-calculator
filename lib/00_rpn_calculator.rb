class RPNCalculator
  require 'byebug'

  def initialize
    @stack = []
  end

  def push(num)
    @stack.push(num)
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def value
    @stack.last
  end

  def tokens(str)
    str.split.map { |token| operation?(token) ? token.to_sym : token.to_i }
  end

  def evaluate(str)
    @stack = []
    tokens = tokens(str)
    tokens.each { |token| operation?(token) ? perform_operation(token) : push(token) }
    value
  end

  private

  def operation?(str)
    %i(+ - * /).include?(str.to_s.to_sym)
  end

  def perform_operation(operator)
    second_op = @stack.pop
    first_op = @stack.pop

    raise "calculator is empty" if first_op.nil? || second_op.nil?

    case operator
    when :+
      @stack << first_op + second_op
    when :-
      @stack << first_op - second_op
    when :/
      @stack << first_op.to_f / second_op
    when :*
      @stack << first_op * second_op
    else
      @stack << first_op
      @stack << second_op
      raise "Operator #{operator} not supported"
    end
  end
end
